# platform-data-pipelines

This is a conceptual repo layout that mixes a
 monorepo strategy with third party pipelines contributed as git submodules.

In this example we have a shared codebase: `dags` and `tests`. We could centralize CI and enforce code standards to all
projects.

Project code is added via `git submodules`. Add a new project with
```
$ git submodule add <remote_url> <destination_dir>
```

When cloning the repo, submodules need to be initialized. The following will clone the pipeline repo, and recursively
fetch all submodules it includes:
```
git clone --recurse-submodules git@gitlab.wikimedia.org:gmodena/platform-data-pipelines.git
```
